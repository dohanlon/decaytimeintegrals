# From https://gitlab.cern.ch/snippets/1303

import numpy as np
import cmath

from scipy.special import erf, erfc, wofz

from scipy.fftpack import fft, ifft, fft2, ifft2

from scipy.integrate import quad
# from scipy.signal import convolve
# from scipy.signal.windows import gaussian

# Much better behaved
from astropy.convolution import convolve
from astropy.convolution import Gaussian1DKernel

from scipy.interpolate import CubicSpline

# Args must be (integration parameter, other args)
def cosTerm(t, Gamma, deltaM):
    return np.exp(-Gamma * t) * np.cos(deltaM * t)

def sinTerm(t, Gamma, deltaM):
    return np.exp(-Gamma * t) * np.sin(deltaM * t)

def paperSinCosWithResolution(Gamma, deltaM, mu, sigma, t):

    x = (t - mu) / (np.sqrt(2) * sigma)

    z = (( Gamma - 1j * deltaM ) * sigma) / np.sqrt(2)

    arg = 1j * (z - x)

    conv = 0.5 * np.exp(-x ** 2) * wofz(arg)

    cos_conv = conv.real
    sin_conv = conv.imag

    return cos_conv, sin_conv

def normalIntegralCos(Gamma, deltaM, t0, t1):

    n1 = np.exp(-Gamma * t1) * (deltaM * np.sin(deltaM * t1) - Gamma * np.cos(deltaM * t1))
    n0 = np.exp(-Gamma * t0) * (deltaM * np.sin(deltaM * t0) - Gamma * np.cos(deltaM * t0))

    d = deltaM ** 2 + Gamma ** 2

    return (n1 - n0) / d

def normalIntegralSin(Gamma, deltaM, t0, t1):

    n1 = -np.exp(-Gamma * t1) * (Gamma * np.sin(deltaM * t1) + deltaM * np.cos(deltaM * t1))
    n0 = -np.exp(-Gamma * t0) * (Gamma * np.sin(deltaM * t0) + deltaM * np.cos(deltaM * t0))

    d = deltaM ** 2 + Gamma ** 2

    return (n1 - n0) / d

def paperIntegralSinCos(Gamma, deltaM, t0, t1):

    n1 = -np.exp(-(Gamma - 1j * deltaM) * t1)
    n0 = -np.exp(-(Gamma - 1j * deltaM) * t0)

    d = Gamma - 1j * deltaM

    return (n1 - n0) / d

def paperIntegralSinCosWithResolution(Gamma, deltaM, t0, t1, mu, sigma):

    x1 = (t1 - mu) / (np.sqrt(2) * sigma)
    x0 = (t0 - mu) / (np.sqrt(2) * sigma)

    z = (( Gamma - 1j * deltaM ) * sigma) / np.sqrt(2)

    arg1 = 1j * (z - x1)
    arg0 = 1j * (z - x0)

    integral = erf(x1) - np.exp(-(x1*x1)) * wofz(arg1)
    integral -= erf(x0) - np.exp(-(x0*x0)) * wofz(arg0)
    integral *= (sigma / (2 * np.sqrt(2) * z))

    cos_integral = integral.real
    sin_integral = integral.imag

    return cos_integral, sin_integral

def coshTerm(t, Gamma, deltaG):
    return np.exp(-Gamma * t) * np.cosh(deltaG * t / 2.)

def sinhTerm(t, Gamma, deltaG):
    return np.exp(-Gamma * t) * np.sinh(deltaG * t / 2.)

def paperSinhCoshWithResolution(Gamma, deltaG, t, mu, sigma):

    x = (t - mu) / (np.sqrt(2) * sigma)

    z_H = ((Gamma - deltaG / 2.) * sigma) / np.sqrt(2)
    z_L = ((Gamma + deltaG / 2.) * sigma) / np.sqrt(2)

    arg_H = 1j * (z_H - x)
    arg_L = 1j * (z_L - x)

    conv_H = 0.5 * np.exp(-x ** 2) * wofz(arg_H)
    conv_L = 0.5 * np.exp(-x ** 2) * wofz(arg_L)

    cosh_conv = 0.5 * (conv_H + conv_L)
    sinh_conv = 0.5 * (conv_H - conv_L)

    return cosh_conv, sinh_conv

def normalIntegralCosh(Gamma, deltaG, t0, t1):

    n1 = -2 * np.exp(-Gamma * t1) * (2 * Gamma * np.cosh((deltaG * t1) / 2.) + deltaG * np.sinh((deltaG * t1) / 2.))
    n0 = -2 * np.exp(-Gamma * t0) * (2 * Gamma * np.cosh((deltaG * t0) / 2.) + deltaG * np.sinh((deltaG * t0) / 2.))

    d = 4 * Gamma ** 2 - deltaG ** 2

    return (n1 - n0) / d

def normalIntegralSinh(Gamma, deltaG, t0, t1):

    n1 = -2 * np.exp(-Gamma * t1) * (2 * Gamma * np.sinh((deltaG * t1) / 2.) + deltaG * np.cosh((deltaG * t1) / 2.))
    n0 = -2 * np.exp(-Gamma * t0) * (2 * Gamma * np.sinh((deltaG * t0) / 2.) + deltaG * np.cosh((deltaG * t0) / 2.))

    d = 4 * Gamma ** 2 - deltaG ** 2

    return (n1 - n0) / d

def paperIntegralSinhCosh(Gamma, deltaG, t0, t1):

    GammaH = Gamma - 0.5*deltaG
    GammaL = Gamma + 0.5*deltaG

    nL1 = -np.exp(-GammaL * t1) / GammaL
    nH1 = -np.exp(-GammaH * t1) / GammaH
    nL0 = -np.exp(-GammaL * t0) / GammaL
    nH0 = -np.exp(-GammaH * t0) / GammaH

    cosh_integral = 0.5 * ( (nH1+nL1) - (nH0+nL0) )
    sinh_integral = 0.5 * ( (nH1-nL1) - (nH0-nL0) )

    return cosh_integral, sinh_integral

def paperIntegralSinhCoshWithResolution(Gamma, deltaG, t0, t1, mu, sigma):

    x1 = (t1 - mu) / (np.sqrt(2) * sigma)
    x0 = (t0 - mu) / (np.sqrt(2) * sigma)

    z_H = ((Gamma - deltaG / 2.) * sigma) / np.sqrt(2)

    arg1_H = 1j * (z_H - x1)
    arg0_H = 1j * (z_H - x0)

    integral_H = erf(x1) - np.exp(-(x1*x1)) * wofz(arg1_H)
    integral_H -= erf(x0) - np.exp(-(x0*x0)) * wofz(arg0_H)
    integral_H *= (sigma / (2 * np.sqrt(2) * z_H))

    z_L = ((Gamma + deltaG / 2.) * sigma) / np.sqrt(2)

    arg1_L = 1j * (z_L - x1)
    arg0_L = 1j * (z_L - x0)

    integral_L = erf(x1) - np.exp(-(x1*x1)) * wofz(arg1_L)
    integral_L -= erf(x0) - np.exp(-(x0*x0)) * wofz(arg0_L)
    integral_L *= (sigma / (2 * np.sqrt(2) * z_L))

    cosh_integral = 0.5 * (integral_H + integral_L)
    sinh_integral = 0.5 * (integral_H - integral_L)

    return cosh_integral, sinh_integral

def convolveAndIntegrate(xs, data, kernel, t0, t1):

    convolved = convolve(data, kernel)
    splineConvolved = CubicSpline(xs, convolved)
    convolvedIntegral = quad(splineConvolved, t0, t1)

    return convolvedIntegral[0]

def faddExp(Gamma, deltaM, t0, t1, mu, sigma):

    # For dt integral terms

    x1 = (t1 - mu) / (np.sqrt(2) * sigma)
    x0 = (t0 - mu) / (np.sqrt(2) * sigma)

    z = (( Gamma - 1j * deltaM ) * sigma) / np.sqrt(2)

    arg1 = 1j * (z - x1)
    arg0 = 1j * (z - x0)

    f0 = wofz(arg0)
    f1 = wofz(arg1)

    return np.exp(-(x0*x0)), f0.real, f0.imag, np.exp(-(x1*x1)), f1.real, f1.imag


def plotFaddeevaTerms():

    # Gamma = 1 / tau, for tau = approx 1.5
    Gamma = 0.66
    # values of deltaM and deltaG provided for either Bs or Bd

    # Bs
    # deltaM = 26.89*Gamma
    # deltaG = 0.129*Gamma

    # Bd
    deltaM = 0.769*Gamma
    deltaG = 0.001*Gamma

    # decay time range
    t1 = 20.0
    t0 = 0.0

    # resolution function parameters
    mean = 0.0
    sigma = 1.0

    sigmas = np.linspace(0.01, 50, 1000)

    vals = [faddExp(Gamma, deltaM, t0, t1, 0, s) for s in sigmas]

    p = plt.plot(sigmas, vals)

    plt.legend(p, ['np.exp(-(x0 ** 2))',
                    'Re(wofz(arg0))',
                    'Im(wofz(arg0))',
                    'np.exp(-(x1 ** 2))',
                    'Re(wofz(arg1))',
                    'Im(wofz(arg1))'],
                loc = 0)
    plt.xlabel('Resolution (ps)')

    plt.ylim(-2, 2)
    plt.savefig('sigmasTest.pdf')

    plt.xlim(0, 10)
    plt.ylim(-1E-3, 1E-3)
    plt.savefig('sigmasTest0.pdf')

    plt.ylim(1 + -1E-3, 1 + 1E-3)
    plt.savefig('sigmasTest1.pdf')

    plt.ylim(-2, 2)
    plt.xlim(0, 5)
    plt.savefig('sigmasTestLow.pdf')


def comparePDFs():

    # Gamma = 1 / tau, for tau = approx 1.5
    Gamma = 0.66
    # values of deltaM and deltaG provided for either Bs or Bd

    # Bs
    # deltaM = 26.89*Gamma
    # deltaG = 0.129*Gamma

    # Bd
    deltaM = 0.769*Gamma
    deltaG = 0.001*Gamma

    # decay time range
    t1 = 20.0
    t0 = 0.0

    # resolution function parameters
    mean = 0.0
    sigma = 5

    # Gamma /= 10. # Test

    # Samples for convolution/spline
    samples = 1000

    print(normalIntegralCos(Gamma, deltaM, t0, t1))
    print(normalIntegralSin(Gamma, deltaM, t0, t1))
    print('')

    numericalIntCos = quad(cosTerm, t0, t1, args = (Gamma, deltaM))
    numericalIntSin = quad(sinTerm, t0, t1, args = (Gamma, deltaM))
    print(numericalIntCos[0])
    print(numericalIntSin[0])
    print('')

    print(normalIntegralCos(Gamma, deltaM, t0, t1))
    print(normalIntegralSin(Gamma, deltaM, t0, t1))
    print('')

    sincosint = paperIntegralSinCos(Gamma, deltaM, t0, t1)
    print(sincosint.real)
    print(sincosint.imag)
    print('')

    cosint, sinint = paperIntegralSinCosWithResolution(Gamma, deltaM, t0, t1, mean, sigma)
    print(cosint)
    print(sinint)
    print('')

    print(normalIntegralCosh(Gamma, deltaG, t0, t1))
    print(normalIntegralSinh(Gamma, deltaG, t0, t1))
    print('')

    numericalIntCosh = quad(coshTerm, t0, t1, args = (Gamma, deltaG))
    numericalIntSinh = quad(sinhTerm, t0, t1, args = (Gamma, deltaG))
    print(numericalIntCosh[0])
    print(numericalIntSinh[0])
    print('')

    coshint, sinhint = paperIntegralSinhCosh(Gamma, deltaG, t0, t1)
    print(coshint)
    print(sinhint)
    print('')

    coshint, sinhint = paperIntegralSinhCoshWithResolution(Gamma, deltaG, t0, t1, mean, sigma)
    print(coshint.real)
    print(sinhint.real)
    print('')

    s = (t1 - t0) / samples
    splineXs = np.linspace(t0, t1, samples)
    kernel = Gaussian1DKernel(stddev=sigma / s, mode = 'oversample')

    convSinInt = convolveAndIntegrate(splineXs, sinTerm(splineXs, Gamma, deltaM), kernel, t0, t1)
    convCosInt = convolveAndIntegrate(splineXs, cosTerm(splineXs, Gamma, deltaM), kernel, t0, t1)

    convSinhInt = convolveAndIntegrate(splineXs, sinhTerm(splineXs, Gamma, deltaG), kernel, t0, t1)
    convCoshInt = convolveAndIntegrate(splineXs, coshTerm(splineXs, Gamma, deltaG), kernel, t0, t1)

    print(convSinInt)
    print(sinint)

    print('')

    print(convCosInt)
    print(cosint)

    print('')

    print(convSinhInt)
    print(sinhint)

    print('')

    print(convCoshInt)
    print(coshint)

    convFaddCosSin = paperSinCosWithResolution(Gamma, deltaM, 0, sigma, splineXs)

    convFaddCos = convFaddCosSin[0]
    convFaddSin = convFaddCosSin[1]

    convManualSin = convolve(sinTerm(splineXs, Gamma, deltaM), kernel)
    convManualCos = convolve(cosTerm(splineXs, Gamma, deltaM), kernel)

    plt.plot(splineXs, convFaddCos - convManualCos, lw = 1.0)
    plt.plot(splineXs, convFaddSin - convManualSin, lw = 1.0)
    plt.savefig('convTestCosSin.pdf')
    plt.clf()

    convFaddCoshSinh = paperSinhCoshWithResolution(Gamma, deltaG, splineXs, 0, sigma)
    convFaddCosh = convFaddCoshSinh[0]
    convFaddSinh = convFaddCoshSinh[1]

    convManualSinh = convolve(sinhTerm(splineXs, Gamma, deltaG), kernel)
    convManualCosh = convolve(coshTerm(splineXs, Gamma, deltaG), kernel)

    plt.plot(splineXs, convFaddCosh - convManualCosh, lw = 1.0)
    plt.plot(splineXs, convFaddSinh - convManualSinh, lw = 1.0)
    plt.savefig('convTestCoshSinh.pdf')
    plt.clf()

if __name__ == '__main__':

    import matplotlib.pyplot as plt

    from matplotlib import rcParams
    import matplotlib as mpl
    mpl.use('Agg')

    plt.style.use(['seaborn-whitegrid', 'seaborn-ticks'])
    import matplotlib.ticker as plticker
    rcParams['figure.figsize'] = 12, 8
    rcParams['axes.facecolor'] = 'FFFFFF'
    rcParams['savefig.facecolor'] = 'FFFFFF'
    rcParams['figure.facecolor'] = 'FFFFFF'

    rcParams['xtick.direction'] = 'in'
    rcParams['ytick.direction'] = 'in'

    rcParams['mathtext.fontset'] = 'cm'
    rcParams['mathtext.rm'] = 'serif'

    rcParams.update({'figure.autolayout': True})

    plotFaddeevaTerms()
