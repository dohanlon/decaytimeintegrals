# DecayTimeIntegrals

Decay-time integral calculations for Bd and Bs decays with numerical resolution convolution. Verification of the implementation with the Faddeeva method (from https://arxiv.org/abs/1407.0748).
